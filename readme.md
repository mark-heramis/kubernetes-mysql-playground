# Kubernetest Playground with MySQL and PHPMyAdmin

# Prerequisites

- install [minikube](https://minikube.sigs.k8s.io/docs/start/)
- install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

# Objective
1. Create a [MySQL](https://hub.docker.com/_/mysql) database [pod](https://kubernetes.io/docs/concepts/workloads/pods/).
2. Create a [PhpMyAdmin](https://hub.docker.com/_/phpmyadmin) [pod](https://kubernetes.io/docs/concepts/workloads/pods/).
3. The MySQL instance must not be available to the public.
4. The PhpMyAdmin Service has to be accessable to to the public.

# Files

- **mysql-secret.yaml**: a [kubernetes secret](https://kubernetes.io/docs/concepts/configuration/secret/) file used for storing confidential information such as **username** and/or **passwords**.

- **mysql.yaml**: Create a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) and a [Service](https://kubernetes.io/docs/concepts/services-networking/service/) for the [MySql](https://hub.docker.com/_/mysql) database that will only be used internally.

- **mysql-configmap.yaml**: create a [configMap](https://kubernetes.io/docs/concepts/configuration/configmap/) used to store reusable configuration files to store configurations that are not necessarily confidential information.

- **phpmyadmin.yaml**: Create a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) and a [Service](https://kubernetes.io/docs/concepts/services-networking/service/) for the [PhpMyAdmin](https://hub.docker.com/_/phpmyadmin) that will connect to the **internal** MongoDB instance and is also public facing.

# Commands

This commands will create/update the `secret`, `configmap`, `deployment` and `service`.

```bash
kubectl apply -f mysql-secret.yaml
kubectl apply -f mysql.yaml
kubectl apply -f mysql-configmap.yaml
kubectl apply -f phpmyadmin.yaml
```

Since we're using the minikube version (offline version) of kubernetes, the `phpmyadmin-service` (which is the service we created in the `phpmyadmin.yaml`) should not be getting a public IP address (you can only get that when its online).

You can verify this with the command below

```bash
kubectl get service
```

after this command, you should get a list of services, and the `phpmyadmin-service` service should not have an `EXTERNAL-IP`.

However to see if things are propperly working, you can emmulate a public IP with the following command:

```bash
minikube service phpmyadmin-service
```

**Also**... check this [kubernetes cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) and [minikube cheatsheet](https://cheatsheet.dennyzhang.com/cheatsheet-minikube-a4) document, it will be useful.